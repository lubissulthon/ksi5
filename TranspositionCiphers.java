import java.util.Arrays;
import java.util.Scanner;

public class TranspositionCiphers {

    private static Scanner in;

    public static void main(String[] args) {
        System.out.println("Columnar Transposition Cipher");
        in = new Scanner(System.in);

        System.out.print("1. Enkripsi\n2. Dekripsi\nPilih (1, 2): ");
        int choice = in.nextInt();
        in.nextLine();

        if (choice == 1) {
            System.out.println("Enkripsi");
            enkripsi();

        } else if (choice == 2) {
            System.out.println("Dekripsi");
            dekripsi();

        } else {
            System.out.println("Pilihan Tidak Valid");
            System.exit(0);
        }
    }

    private static void enkripsi() {
        System.out.print("Masukkan Pesan: ");
        String plainText = in.nextLine().toUpperCase().replace(" ", "");
        StringBuilder msg = new StringBuilder(plainText);
    
        System.out.print("Masukkan Kunci: ");
        String keyword = in.nextLine().toUpperCase();
    
        int[] kywrdNumList = keywordNumAssign(keyword);
    
        for (int i = 0, j = 1; i < keyword.length(); i++, j++) {
            System.out.print(keyword.substring(i, j) + " ");
        }
        System.out.println();
    
        for (int i : kywrdNumList) {
            System.out.print(i + " ");
        }
    
        System.out.println();
        System.out.println("-------------------------");
    
        int extraLetters = msg.length() % keyword.length();
        int dummyCharacters = keyword.length() - extraLetters;
    
        if (extraLetters != 0) {
            for (int i = 0; i < dummyCharacters; i++) {
                msg.append(".");
            }
        }
    
        int numOfRows = msg.length() / keyword.length();
    
        char[][] arr = new char[numOfRows][keyword.length()];
    
        int z = 0;
        for (int i = 0; i < numOfRows; i++) {
            for (int j = 0; j < keyword.length(); j++) {
                arr[i][j] = msg.charAt(z);
                z++;
            }
        }
    
        for (int i = 0; i < numOfRows; i++) {
            for (int j = 0; j < keyword.length(); j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    
        StringBuilder cipherText = new StringBuilder();
    
        System.out.println();
        String numLoc = getNumberLocation(keyword, kywrdNumList);
        System.out.println("Lokasi angka: " + numLoc);
        System.out.println();
    
        for (int i = 0, k = 0; i < numOfRows+1; i++, k++) {
            int d;
            if (k == keyword.length()) {
                break;
            } else {
                d = Character.getNumericValue(numLoc.charAt(k));
            }
            for (int j = 0; j < numOfRows; j++) {
                if (arr[j][d] != '.') {  // tambahkan kondisi untuk memeriksa karakter '.' sebelum ditambahkan
                    cipherText.append(arr[j][d]);
                } 
            }
        }
    
        System.out.println("Teks Terenkripsi: " + cipherText);
    
    }
    

    private static void dekripsi() {
    System.out.print("Masukkan Pesan Terenkripsi: ");
    String cipherText = in.nextLine().toUpperCase().replace(" ", "");

    System.out.print("Masukkan Kunci: ");
    String keyword = in.nextLine().toUpperCase();

    // Validasi panjang kunci
    if (keyword.length() <= 1) {
        System.out.println("Kunci terlalu pendek. Pilih kunci yang lebih panjang.");
        System.exit(0);
    }

    // Validasi huruf unik pada kunci
    for (int i = 0; i < keyword.length() - 1; i++) {
        for (int j = i + 1; j < keyword.length(); j++) {
            if (keyword.charAt(i) == keyword.charAt(j)) {
                System.out.println("Kunci tidak boleh mengandung huruf yang sama. Pilih kunci lain.");
                System.exit(0);
            }
        }
    }

    // Hitung jumlah baris yang dibutuhkan dalam matriks
    int numRows = (int) Math.ceil((double) cipherText.length() / keyword.length());

    // Hitung jumlah karakter padding
    int padding = numRows * keyword.length() - cipherText.length();

    // Inisialisasi matriks
    char[][] matrix = new char[numRows][keyword.length()];

    // Permutasi kolom sesuai dengan urutan kunci
    char[] keyArray = keyword.toCharArray();
    Arrays.sort(keyArray);

    int index = 0;
    for (char c : keyArray) {
        int col = keyword.indexOf(c);
        for (int row = 0; row < numRows; row++) {
            if (row == numRows - 1 && col >= keyword.length() - padding) {
                // Skip karakter padding pada baris terakhir
                continue;
            }
            matrix[row][col] = cipherText.charAt(index++);
        }
    }

    // Baca matriks dan hasilkan teks terdekripsi
    StringBuilder plainText = new StringBuilder();
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < keyword.length(); j++) {
            plainText.append(matrix[i][j]);
        }
    }

    System.out.println("Teks Terdekripsi: " + plainText);
}

    
    private static String getNumberLocation(String keyword, int[] kywrdNumList) {
        StringBuilder numLoc = new StringBuilder();
        for (int i = 1; i < keyword.length() + 1; i++) {
            for (int j = 0; j < keyword.length(); j++) {
                if (kywrdNumList[j] == i) {
                    numLoc.append(j);
                }
            }
        }
        return numLoc.toString();
    }

    private static int[] keywordNumAssign(String keyword) {
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int[] kywrdNumList = new int[keyword.length()];

        int init = 0;
        for (int i = 0; i < alpha.length(); i++) {
            for (int j = 0; j < keyword.length(); j++) {
                if (alpha.charAt(i) == keyword.charAt(j)) {
                    init++;
                    kywrdNumList[j] = init;
                }
            }
        }
        return kywrdNumList;
    }
}
