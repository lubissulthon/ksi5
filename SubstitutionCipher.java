import java.util.Scanner;

public class SubstitutionCipher {

    private static Scanner in;

    public static void main(String[] args) {
        System.out.println("Substitution Cipher");
        in = new Scanner(System.in);

        System.out.print("1. Enkripsi\n2. Dekripsi\nPilih (1, 2): ");
        int choice = in.nextInt();
        in.nextLine();

        if (choice == 1) {
            System.out.println("Enkripsi");
            enkripsi();

        } else if (choice == 2) {
            System.out.println("Dekripsi");
            dekripsi();

        } else {
            System.out.println("Pilihan Tidak Valid");
            System.exit(0);
        }
    }

    private static void enkripsi() {
        System.out.print("Masukkan Pesan: ");
        String plainText = in.nextLine().toUpperCase().replace(" ", "");

        // Substitusi huruf berdasarkan aturan tertentu
        String cipherText = substitusi(plainText, 3); // Gunakan nilai shift yang positif

        System.out.println("Teks Terenkripsi: " + cipherText);
    }

    private static void dekripsi() {
        System.out.print("Masukkan Pesan Terenkripsi: ");
        String cipherText = in.nextLine().toUpperCase().replace(" ", "");

        // Substitusi huruf berdasarkan aturan tertentu (reverse substitution)
        String plainText = substitusi(cipherText, -3); // Gunakan nilai shift yang negatif

        System.out.println("Teks Terdekripsi: " + plainText);
    }

    private static String substitusi(String text, int shift) {
        // Misalnya, substitusi dengan aturan A->D, B->E, C->F, ..., Z->C
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char currentChar = text.charAt(i);
            if (Character.isLetter(currentChar)) {
                // Substitusi hanya dilakukan untuk karakter huruf
                char substitutedChar = (char) ('A' + (currentChar - 'A' + shift + 26) % 26);
                result.append(substitutedChar);
            } else {
                // Karakter selain huruf tetap dipertahankan
                result.append(currentChar);
            }
        }
        return result.toString();
    }
}
